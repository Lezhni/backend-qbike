<?php

error_reporting(E_ALL);

session_start();

require_once __DIR__.'/vendor/autoload.php';
require_once __DIR__.'/db.php';

//Initializing router
$router = new AltoRouter();

//Initializing DB
$db = new DB();

//Initializing Twig
$loader = new Twig_Loader_Filesystem(__DIR__.'/templates');
$twig = new Twig_Environment($loader, array(
    //'cache' => __DIR__.'/templates/cache'
));

//Middleware Auth
function is_logged() {
	if ($_SESSION['auth'] != 1) {
		redirect('Location: /auth');
	}
}
//Routes
//Index page
$router->map('GET', '/', function() use ($twig) {
	$page = $twig->render('index.twig');
	echo $template = $twig->render('template.twig', array(
		'content' => $page
	));
});
//Registration, frontend
$router->map('GET', '/reg', function() use ($twig) {
	$page = $twig->render('registration.twig');
	echo $template = $twig->render('template.twig', array(
		'content' => $page
	));
});
//Registration, backend
$router->map('POST', '/reg', function() use ($db) {
	if ($_POST) {
		$userData = array(
			'name' => $_POST['name'],
			'login' => $_POST['login'],
			'password' => md5($_POST['password'])
		);
		$db->regUser($userData);
		header('Location: /auth', 301);
	} else {
		$page = $twig->render('registration.twig', array(
			'message' => 'Одно или больше полей не заполнены',
			'messageClass' => 'error-bg'
		));
		echo $template = $twig->render('template.twig', array(
			'content' => $page
		));
	}
});
//Authorization, frontend
$router->map('GET', '/auth', function() use ($twig) {
	$page = $twig->render('authorization.twig');
	echo $template = $twig->render('template.twig', array(
		'content' => $page
	));
});
//Authorization, logout
$router->map('GET', '/auth/logout', function() {
	unset($_SESSION['userId']);
	$_SESSION['auth'] = false;
	header('Location: /auth');
});
//Authorization, backend
$router->map('POST', '/auth', function() use ($db, $twig) {
	if ($_POST) {
		$login = $_POST['login'];
		$pass = md5($_POST['password']);
		$user = $db->authUser($login, $pass);
		if ($user) {
			$_SESSION['auth'] = true;
			$_SESSION['userId'] = $user->id;
			header('Location: /profile');
		} else {
			$page = $twig->render('authorization.twig', array(
				'message' => 'Неверный логин или пароль, попробуйте снова',
				'messageClass' => 'error-bg'
			));
			echo $template = $twig->render('template.twig', array(
				'content' => $page
			));
		}
	} else {
		$page = $twig->render('authorization.twig', array(
			'message' => 'Неверный логин или пароль, попробуйте снова',
			'messageClass' => 'error-bg'
		));
		echo $template = $twig->render('template.twig', array(
			'content' => $page
		));
	}
});
//Profile page
$router->map('GET', '/profile', function() use ($db, $twig) {
	is_logged();
	$userId = $_SESSION['userId'];
	$userData = $db->getUser($userId);
	$teamData = $db->getTeamByUser($userId);
	$teamMembers = $db->getTeamMembersByUser($userId);
	$page = $twig->render('profile.twig', array(
		'profile' => $userData,
		'team' => $teamData,
		'teamMembers' => $teamMembers
	));
	echo $template = $twig->render('template.twig', array(
		'content' => $page
	));
});
//Profile edit team
$router->map('GET', '/profile/edit', function() use ($db, $twig) {
	is_logged();
	$userId = $_SESSION['userId'];
	$userData = $db->getUser($userId);
	$page = $twig->render('profileEdit.twig', array(
		'profile' => $userData
	));
	echo $template = $twig->render('template.twig', array(
		'content' => $page
	));
});
//Profile edit team, update data
$router->map('POST', '/profile/edit', function() use ($db, $twig) {
	if ($_POST) {
		$data['name'] = $_POST['name'];
		$data['info'] = $_POST['info'];
		//Upload avatar to server
		$filepath = dirname(__FILE__);
		$filename = '/media/img/avatars/'.basename($_FILES['avatar']['name']);
		if (move_uploaded_file($_FILES['avatar']['tmp_name'], $filepath.$filename)) {
		    $data['img'] = $filename;
		}
		$userId = $_SESSION['userId'];
		$updateUser = $db->updateUser($data, $userId);
		$userData = $db->getUser($userId);
		$page = $twig->render('profileEdit.twig', array(
			'profile' => $userData,
			'message' => 'Профиль успешно обновлен',
			'messageClass' => 'success-bg'
		));
		echo $template = $twig->render('template.twig', array(
			'content' => $page
		));
	} else {
		header('Location: /profile/edit', 301);
	}
});
//Search team page
$router->map('GET', '/search-team', function() use ($db, $twig) {
	is_logged();
	$tName = $_GET['q'] ? $_GET['q'] : '';
	$searchResult = $db->searchTeam($tName);
	$page = $twig->render('searchTeam.twig', array(
		'results' => $searchResult,
		'query' => $tName
	));
	echo $template = $twig->render('template.twig', array(
		'content' => $page
	));
});
//Create team page
$router->map('GET', '/create-team', function() use ($twig) {
	is_logged();
	$page = $twig->render('createTeam.twig');
	echo $template = $twig->render('template.twig', array(
		'content' => $page
	));
});
//Create team, backend
$router->map('POST', '/create-team', function() use ($db) {
	if ($_POST) {
		$data['name'] = $_POST['name'];
		$data['description'] = $_POST['desc'];
		$team = $db->createTeam($data);
		if ($team) {
			$dataU['user_id'] = $_SESSION['userId'];
			$team = $_POST['name'];
			$dataU['is_captain'] = 1;
			$db->addToTeam($dataU, $team);
			header('Location: /profile');
		}
	} else {
		header('Location: /profile');
	}
});
//Request in team page
$router->map('GET', '/request-in-team', function() use ($db, $twig) {
	is_logged();
	$userId = $_SESSION['userId'];
	$teamData = $db->getTeamByUser($userId);
	//Is user captain
	if ($teamData->is_captain != 1) {
		header('Location: /profile');
	}
	$users = $db->getFreeUsers();
	foreach ($users as $key => $user) {
		if ($user->id == $_SESSION['userId']) {
			unset($users[$key]);
		}
	}
	$page = $twig->render('requestInTeam.twig', array(
		'users' => $users
	));
	echo $template = $twig->render('template.twig', array(
		'content' => $page
	));
});

//Router magic
$match = $router->match();
if( $match && is_callable( $match['target'] ) ) {
	call_user_func_array( $match['target'], $match['params'] ); 
} else {
	header( $_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');
}
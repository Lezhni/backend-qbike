$(document).ready(function() {

	$('.leftbar').css('left', -($('.leftbar').width()) - 50);

	$('.menu-btn').click(function() {

		if ($(this).hasClass('leftbar-opened')) {
			$(this).removeClass('leftbar-opened');
			$('.shadow').hide();
			$('.leftbar').animate({
				left: -($('.leftbar').width() + 50)
			}, 500);
		} else {
			$(this).addClass('leftbar-opened');
			$('.shadow').show();
			$('.leftbar').animate({
				left: 0
			}, 500);
		}

		return false;
	});

});
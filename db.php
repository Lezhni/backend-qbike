<?php

class DB {

	protected $dbConfig = array(
		'driver'    => 'pdo', // Db driver
	    'host'      => 'localhost',
	    'database'  => 'qbike',
	    'username'  => 'root',
	    'password'  => '',
	    'charset'   => 'utf8', // Optional
	    'collation' => 'utf8_unicode_ci', // Optional
	    'prefix'    => '', // Table prefix, optional
	    'options'   => array( // PDO constructor options, optional
	        PDO::ATTR_TIMEOUT => 5,
	        PDO::ATTR_EMULATE_PREPARES => false,
	    ),
	);
	public function __construct() {
		new \Pixie\Connection('mysql', $this->dbConfig, 'QB');
	}
	public function getUser($id) {
		$query = QB::table('users')->where('id', '=', $id);
		$user = $query->first();
		return $user;
	}
	public function getUsers() {
		$query = QB::table('users');
		$users = $query->get();
		return $users;
	}
	public function getFreeUsers() {
		$subQuery = QB::table('requests')
		->select('receiver_id');
		$query = QB::query("SELECT * 
			FROM `users` 
			WHERE `users`.`id` not IN 
			(SELECT `receiver_id` 
				FROM `requests`
			)");
		$users = $query->get();
		return $users;
	}
	public function regUser($data) {
		$query = QB::table('users');
		$newUser = $query->insert($data);
		return $newUser;
	}
	public function authUser($login, $pass) {
		$query = QB::table('users')
		->where('login', '=', $login)
		->where('password', '=', $pass);
		$user = $query->first();
		return $user;
	}
	public function updateUser($data, $user) {
		$query = QB::table('users')
		->where('id', '=', $user)
		->update($data);
	}
	public function getTeam($id) {
		$query = QB::table('teams')->where('id', '=', $id);
		$team = $query->first();
		return $team;
	}
	public function getTeams() {
		$query = QB::table('teams');
		$teams = $query->get();
		return $teams;
	}
	public function createTeam($data) {
		$query = QB::table('teams');
		$newTeam = $query->insert($data);
		return $newTeam;
	}
	public function getTeamByUser($id) {
		$query = QB::table('teams')
		->select(array('teams.name', 'teams.description'))
		->join('users_m2m_teams', 'users_m2m_teams.team_id', '=', 'teams.id')
		->join('users', 'users.id', '=', 'users_m2m_teams.user_id')
		->where('users.id', '=', $id);
		$team = $query->first();
		if ($team) {
			$queryCaptain = QB::table('users_m2m_teams')
			->select('is_captain')
			->where('user_id', '=', $id);
			$queryCaptain = $queryCaptain->first();
			$team->is_captain = $queryCaptain->is_captain;
		}
		return $team;
	}
	public function getTeamMembers($id) {
		$query = QB::table('users')
		->select(array('users.login', 'users.img'))
		->join('users_m2m_teams', 'users_m2m_teams.user_id', '=', 'users.id')
		->join('teams', 'teams.id', '=', 'users_m2m_teams.team_id')
		->where('teams.id', '=', $id);
		$members = $query->get();
		return $members;
	}
	public function getTeamMembersByAlias($alias) {
		$query = QB::table('users')
		->select(array('users.login', 'users.img'))
		->join('users_m2m_teams', 'users_m2m_teams.user_id', '=', 'users.id')
		->join('teams', 'teams.id', '=', 'users_m2m_teams.team_id')
		->where('teams.name', '=', $alias);
		$members = $query->get();
		return $members;
	}
	public function getTeamMembersByUser($id) {
		$query = QB::query("SELECT `users`.`img`, `users`.`login`
			FROM `users_m2m_teams`
			INNER JOIN `users` ON `users`.`id` = `users_m2m_teams`.`user_id`
			WHERE `users_m2m_teams`.`team_id` =
			(SELECT `users_m2m_teams`.`team_id`
				FROM `users_m2m_teams`
				WHERE `users_m2m_teams`.`user_id` = ?
			)", array($id));
		$members = $query->get();
		return $members;
	}
	public function searchTeam($name) {
		$query = QB::table('teams')
		->where('name', 'LIKE', "%$name%");
		$teams = $query->get();
		return $teams;
	}
	public function addToTeam($data = array('is_captain' => 0), $team) {
		$subQuery = QB::table('teams')
		->where('name', '=', $team);
		$team = $subQuery->first();
		$query = QB::table('users_m2m_teams');
		$m2m = $query->insert(array(
			'user_id' => $data['user_id'],
			'team_id' => $team->id,
			'is_captain' => $data['is_captain']
		));
		return $m2m;
	}
}